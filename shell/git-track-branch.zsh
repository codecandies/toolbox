if [ "$#" -eq 2 ]; then
    	git checkout -b $1 $2 && git branch -u $2
else
	echo "Takes exactly two arguments: local branchname and remote branchname"
fi
